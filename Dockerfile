from node:alpine

WORKDIR /app

copy guia-tramites-client/ /app

copy guia-tramites-client/.env.production.local /app

run npm install

run npm install -g serve

run npm run build

expose 3000
