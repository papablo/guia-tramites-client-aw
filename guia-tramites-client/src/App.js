import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Login from 'containers/Login/Login'
import HomePage from 'containers/HomePage/HomePage'
import TramiteDetail from 'containers/TramiteDetail/TramiteDetail'
import TramiteDetailAdmin from 'containers/TramiteDetailAdmin/TramiteDetailAdmin'
import AdminDashboard from 'containers/AdminDashboard/AdminDashboard'
import PrivateRoute from 'components/PrivateRoute/PrivateRoute'
import Logout from 'containers/Logout/Logout';

function App() {
    return (
        <Router>
            <Route path="/login" component={Login}></Route>
            <Route path="/logout" component={Logout}></Route>
            <Route exact path="/tramite/:id" component={TramiteDetail}></Route>
            <Route exact path="/" component={HomePage}></Route>
            <PrivateRoute exact path="/tramite/:id/edit" component={TramiteDetailAdmin}></PrivateRoute>
            <PrivateRoute path="/admin" component={AdminDashboard}></PrivateRoute>
        </Router>
    );
}

export default App;
