import React, { Component } from 'react';
import {
    Redirect
} from 'react-router-dom'
import LoginForm from 'components/LoginForm/LoginForm'

class Login extends Component {

    state = {
        redirectToReferrer: false
    }

    login = (data) => {

        localStorage.setItem('api_token', data.api_token)

        this.setState({
            redirectToReferrer:true
        })
    }

    render() {
        let { from } = this.props.location.state || { from: { pathname: "/admin" } }

        let { redirectToReferrer } = this.state

        if (redirectToReferrer) return <Redirect to={from} />

        return (
            <div className="page-content">
                <LoginForm handleSubmit={this.login}/>
            </div>
        )
    }
}

export default Login;
