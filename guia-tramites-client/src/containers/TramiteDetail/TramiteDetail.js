import React, { Component } from 'react';

import "App.css"

import Header from 'components/Header/Header'
import Container from 'react-bootstrap/Container'

import PasoList from 'components/PasoList/PasoList'
import PasoListItem from 'components/PasoListItem/PasoListItem'

import axios from 'axios'

class TramiteDetail extends Component {
    state = {
        tramite: {
        },
        pasos: [ ]
    }

    fetchTramite = () => {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite/'+this.props.match.params.id)
            .then((res) => {
                const tramite = res.data

                this.setState({
                    tramite:tramite
                })

                this.fetchPasos(tramite)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    fetchPasos = (tramite) => {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite/'+this.props.match.params.id+'/pasos')
            .then((res) => {
                const pasos = res.data

                console.log(pasos)
                this.setState({
                    pasos: pasos
                })
            })
            .catch((err) => {
                console.log(err)
            })

    }

    componentDidMount = () => {
        this.fetchTramite()
    }
    render() {

        console.log()
        return (
            <div>
                <Header></Header>
                <div className="page-content">
                    <Container>
                        <h1>{this.state.tramite.titulo}</h1>
                        <h3>{this.state.tramite.descripcion}</h3>
                        <PasoList key={this.state.pasos.tramite_id} pasos={this.state.pasos} pasoComponent={PasoListItem}></PasoList>
                    </Container>
                </div>
            </div>
        );
    }
}

export default TramiteDetail;
