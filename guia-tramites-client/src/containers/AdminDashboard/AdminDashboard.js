import React, { Component } from 'react';

import 'App.css'

import Header from 'components/Header/Header'
import TramiteList from 'components/TramiteList/TramiteList'
import TramiteListItemAdmin from 'components/TramiteListItemAdmin/TramiteListItemAdmin'

import Container from 'react-bootstrap/Container'

import TramiteForm from 'components/TramiteForm/TramiteForm'

import axios from 'axios'

class AdminDashboard extends Component {

    state = {
        tramites: [ ]
    }

    handleSubmit = (tramite) => {
        this.setState({
            tramites:this.state.tramites.concat(tramite)
        })
    }

    fetchTramites() {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite')
            .then((res) => {
                const tramites = res.data
                this.setState({
                    tramites:tramites
                })
            })
            .catch((err) => {console.log('error')})
    }

    componentDidMount(){
        this.fetchTramites()
    }

    handleDelete = (id)=>{
        const tramites = this.state.tramites.filter(tramite => tramite.id !== id)

        this.setState({
            tramites:tramites
        })
    }
    
    render() {
        return (
            <div>
                <Header></Header>
                <div className="page-content">
                    <Container>
                        <h1>Trámites</h1>
                        <h3>Panel de administración</h3>
                        <TramiteForm handleSubmit={this.handleSubmit}></TramiteForm>
                        <TramiteList tramites={this.state.tramites} itemComponent={TramiteListItemAdmin} handleDelete={this.handleDelete}></TramiteList>
                    </Container>
                </div>
            </div>
        );
    }
}

export default AdminDashboard;
