import React, { Component } from 'react';

import "App.css"

import Button from 'react-bootstrap/Button'
import Header from 'components/Header/Header'
import Container from 'react-bootstrap/Container'
import PasoList from 'components/PasoList/PasoList'
import PasoForm from 'components/PasoForm/PasoForm'

import PasoListItemAdmin from 'components/PasoListItemAdmin/PasoListItemAdmin'

import axios from 'axios'

class TramiteDetailAdmin extends Component {
    state = {
        tramite: {
        },
        pasos: [ ]
    }

    fetchTramite = () => {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite/'+this.props.match.params.id)
            .then((res) => {
                const tramite = res.data

                this.setState({
                    tramite:tramite
                })

                this.fetchPasos(tramite)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    fetchPasos = (tramite) => {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite/'+this.props.match.params.id+'/pasos')
            .then((res) => {
                const pasos = res.data

                this.setState({
                    pasos: pasos
                })
            })
            .catch((err) => {
                console.log(err)
            })

    }

    componentDidMount = () => {
        this.fetchTramite()
    }

    handleSubmit = (paso) => {
        console.log(paso)
        this.setState({
            pasos:this.state.pasos.concat(paso)
        })
    }

    handleDelete = (id_paso) => {
        const nPasos = this.state.pasos.filter(paso => paso.id !== id_paso)

        this.setState({
            pasos:nPasos
        })

    }

    render() {
        return (
            <div>
                <Header></Header>
                <div className="page-content">
                    <Button variant="secondary" href="/admin" className="ml-2">Volver al panel</Button>
                    <Container>
                        <h1>{this.state.tramite.titulo}</h1>
                        <h3>{this.state.tramite.descripcion}</h3>

                        <PasoForm tramite={this.state.tramite} handleSubmit={this.handleSubmit}/>

                        <PasoList key={this.state.pasos.tramite_id} pasos={this.state.pasos} handleDelete={this.handleDelete} pasoComponent={PasoListItemAdmin}/>
                    </Container>
                </div>
            </div>
        );
    }
}

export default TramiteDetailAdmin;
