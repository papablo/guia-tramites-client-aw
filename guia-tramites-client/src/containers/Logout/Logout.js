import React, { Component } from 'react'
import Axios from 'axios';

export default class Logout extends Component {

    state = {
        message: null,
        loggedIn: localStorage.getItem('api_token')
    }

    logout = () => {
        if (this.state.loggedIn) {
            Axios.post(process.env.REACT_APP_API_URL+'/api/logout')
                .then((res) => {
                    localStorage.removeItem('api_token')
                    this.setState({
                        message: 'Logout Successfull'
                    })
                })
        }
    }
    render() {
        this.logout()
        return (
            <div>
                <h2>{this.state.message}</h2>
                <a href="/">Go home!</a>
            </div>
        )
    }
}
