import React, { Component } from 'react';

import 'App.css'

import Header from 'components/Header/Header'
import Container from 'react-bootstrap/Container'
import TramiteList from 'components/TramiteList/TramiteList'
import TramiteListItem from 'components/TramiteListItem/TramiteListItem'

import axios from 'axios'

class HomePage extends Component {

    state = {
        tramites: []
    }

    fetchTramites() {
        axios.get(process.env.REACT_APP_API_URL+'/api/tramite')
            .then((res) => {
                const tramites = res.data
                this.setState({
                    tramites:tramites
                })
            })
            .catch((err) => {console.log('error')})
    }

    componentDidMount(){
        this.fetchTramites()
    }

    render() {

        return (
            <div>
                <Header></Header>
                <div className="page-content">
                    <Container>
                        <h1>Trámites</h1>
                        <TramiteList tramites={this.state.tramites} itemComponent={TramiteListItem}></TramiteList>
                    </Container>
                </div>
            </div>
        );
    }
}

export default HomePage;
