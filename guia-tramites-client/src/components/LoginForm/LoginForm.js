import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import './LoginForm.css'
import Axios from 'axios';

export default class LoginForm extends Component {

    handleSubmit = (e) => {
        e.preventDefault()

        const credentials = {
            email: this.email.value,
            password: this.password.value
        }

        Axios.post(process.env.REACT_APP_API_URL+'/api/login', credentials)
    
            .then((res) => {
                const { data } = res.data
                this.props.handleSubmit(data)
            })
            .catch((err) => {
                console.log(err.response)
            })
    }

    render() {
        return (
            <div className="Login">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            required
                            // @ts-ignore
                            ref={email => this.email = email}
                            type="email"
                            placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            required
                            type="password"
                            // @ts-ignore
                            ref={password => this.password = password}
                            placeholder="Password" />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Submit
  </Button>
                </Form>

            </div>
        )
    }
}
