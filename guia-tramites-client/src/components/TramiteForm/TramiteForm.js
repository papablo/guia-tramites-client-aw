import React, { Component } from 'react';

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import axios from 'axios'
import AuthHelper from 'components/AuthHelper/AuthHelper';

class TramiteForm extends Component {

    constructor(props) {
        super(props)

        const tramite = this.props.tramite

        this.state = {
            tramite:tramite,
            titulo: tramite ? tramite.titulo : '',
            descripcion: tramite ? tramite.descripcion : '',
        }
    }

    createTramite = (e) => {

        e.preventDefault()

        const authHelper = new AuthHelper()

        const tramite = {
            titulo:this.titulo.value,
            descripcion:this.descripcion.value
        }

        axios.post(process.env.REACT_APP_API_URL+'/api/tramite',tramite, authHelper.get_auth_headers())
            .then((res)=>{
                this.setState({
                    titulo:'',
                    descripcion:''
                })

                this.props.handleSubmit(res.data)
            })
            .catch((err)=>{
                console.log(err.response)
            })
    }

    updateTramite = (e) => {
        e.preventDefault()

        const authHelper = new AuthHelper();

        const tramite = {
            titulo:this.titulo.value,
            descripcion:this.descripcion.value
        }


        axios.put(process.env.REACT_APP_API_URL+'/api/tramite/'+this.props.tramite.id,tramite, authHelper.get_auth_headers())
            .then((res)=>{
                this.titulo.value = ''
                this.descripcion.value = ''

                this.props.handleSubmit(res.data)

            })
            .catch((err)=>{
                console.log(err)
            })

    }

    tituloChange = (event) => {
        this.setState({
            titulo:event.target.value
        })
    }

    descripcionChange = (event) => {
        this.setState({
            descripcion:event.target.value
        })
    }

    render() {

        const handleSubmit = this.state.tramite ? this.updateTramite : this.createTramite

        return (
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formNombreNuevoTramite">
                    <Form.Control
                        required
                        type="text" 
                        name="titulo" 
                        placeholder="Nombre del nuevo Trámite"
                        ref={titulo => this.titulo = titulo}
                        value={this.state.titulo}
                        onChange={this.tituloChange}
                    />
                </Form.Group>
                <Form.Group controlId="formDescripcionNuevoTramite">
                    <Form.Control
                        as="textarea"
                        name="descripcion"
                        placeholder="Descripción"
                        ref={descripcion => this.descripcion = descripcion}
                        value={this.state.descripcion}
                        onChange={this.descripcionChange}
                    />
                </Form.Group>
                <Button type="submit" variant="success">Crear</Button>
            </Form>
        );
    }
}

export default TramiteForm;
