class AuthHelper {

    isAuthenticated = () => {
        return this.get_token() ? true : false
    }

    get_token = () => {
        return localStorage.getItem('api_token')
    }

    get_auth_headers = () => {
        return {headers:{
            'Authorization': 'Bearer '+this.get_token()
        }}
    } 

}

export default AuthHelper