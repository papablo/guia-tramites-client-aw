import React, { Component } from 'react';

import ListGroup from 'react-bootstrap/ListGroup'

class TramiteListItem extends Component {
    render() {
        return (
            <ListGroup.Item action href={'/tramite/'+this.props.tramite.id}>
                    <h3>{this.props.tramite.titulo}</h3>
                    <p>{this.props.tramite.descripcion}</p>
            </ListGroup.Item>

        );
    }
}

export default TramiteListItem;
