import React, { Component } from 'react';

import ListGroup from 'react-bootstrap/ListGroup'

import Button from 'react-bootstrap/Button'

import {FaTrash, FaEdit} from 'react-icons/fa'

import axios from 'axios'

import TramiteEditModal from 'components/TramiteEditModal/TramiteEditModal'
import AuthHelper from 'components/AuthHelper/AuthHelper';

class TramiteListItemAdmin extends Component {

    state = {
        showModal: false,
        tramite:this.props.tramite
    }

    openModal = () => {
        this.setState({
            showModal: true
        })
    }

    closeModal = () => {
        this.setState({
            showModal: false
        })
    }



    handleDelete = ()=>{

        const authHelper = new AuthHelper()

        axios.delete(process.env.REACT_APP_API_URL+'/api/tramite/'+this.state.tramite.id, authHelper.get_auth_headers())
            .then((res)=>{
                this.props.handleDelete(this.state.tramite.id)
            })
            .catch((err)=>{
                console.log(err.response)
            })

    }

    handleUpdate = (tramite)=> {

        this.setState({
            tramite:tramite,
            showModal:false
        })

    }

    render() {
        return (

            <ListGroup.Item  className="p-0">
                <ListGroup.Item action href={'/tramite/'+this.state.tramite.id+"/edit"}>
                    <h3>{this.state.tramite.titulo}</h3>
                    <p>
                        {this.state.tramite.descripcion}
                    </p>

                    <TramiteEditModal
                        show={this.state.showModal}
                        onHide={this.closeModal}
                        tramite={this.state.tramite}
                        handleSubmit={this.handleUpdate}
                    />

            </ListGroup.Item>
            <Button variant="primary" onClick={event => this.openModal()}>
                <FaEdit></FaEdit>
            </Button>
            <Button variant="danger" style={{float:'right'}} onClick={this.handleDelete} >
                <FaTrash></FaTrash>
            </Button>
        </ListGroup.Item>

        );
    }
}

export default TramiteListItemAdmin;
