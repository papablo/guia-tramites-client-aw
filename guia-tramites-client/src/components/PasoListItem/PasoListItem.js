import React, { Component } from 'react';

import Collapse from 'react-bootstrap/Collapse'
import ListGroup from 'react-bootstrap/ListGroup'

import { FaAngleLeft, FaAngleDown } from 'react-icons/fa'

class PasoListItem extends Component {
    state = {
        open: false
    }

    documento = () => {
        if (this.props.paso.documento) {

            if (this.props.paso.documento.length > 0) {
                return (
                    <div>
                        <hr />
                        Documentación
                    <ul>
                            {this.props.paso.documento.map((documento) => {
                                return (
                                    <li>
                                        <a target="_blank" href={process.env.REACT_APP_API_URL + '' + documento.link}>{documento.nombre}</a>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>)
            }
        }
    }

    render() {

        const getIndicator = () => {
            return this.state.open ? (<FaAngleDown />) : (<FaAngleLeft />)
        }

        return (
            <div>
                <ListGroup.Item
                    onClick={() => this.setState({ open: !this.state.open })}
                    aria-controls="example-collapse-text"
                    aria-expanded={this.state.open}
                >
                    <div>
                        {this.props.paso.nombre}
                        <span style={{ float: 'right' }}>{getIndicator()}</span>
                    </div>

                    <Collapse in={this.state.open}>
                        <div id="example-collapse-text">
                            <hr />

                            {this.props.paso.descripcion}
                            {this.documento()}

                        </div>
                    </Collapse>
                </ListGroup.Item>

            </div>
        );
    }
}

export default PasoListItem;
