import React, { Component } from 'react';

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'

import axios from 'axios'
import AuthHelper from 'components/AuthHelper/AuthHelper';

class PasoForm extends Component {
    constructor(props) {
        super(props)

        // This is for file upload
        // LoI: https://reactjs.org/docs/uncontrolled-components.html#the-file-input-tag
        this.documentacion = React.createRef()

    }

    state = {
        nombre: '',
        descripcion: '',
        message: null
    }

    crearDocumento = (paso, documentacion) => {
        const documento = documentacion[0]

        const data = new FormData()
        data.append('nombre', documento.name)
        data.append('documento', documento)
        data.append('paso_id', paso)

        const authHelper = new AuthHelper();
        console.log(documento)

        return axios.post(
            process.env.REACT_APP_API_URL+'/api/documento',
            data,
            authHelper.get_auth_headers()
        ).then((response) => {
            return response.data
        }).catch((err) => {
            console.log(err.response)
            return err
        })
    }

    crearPaso = (documento) => {
        const paso = {
            tramite_id: this.props.tramite.id,
            orden: this.orden.value,
            nombre: this.nombre.value,
            descripcion: this.descripcion.value,
        }

        const authHelper = new AuthHelper()
        this.orden.value = ''
        this.nombre.value = ''
        this.descripcion.value = ''

        axios.post(process.env.REACT_APP_API_URL+'/api/paso', paso, authHelper.get_auth_headers())
            .then((res) => {
                this.setState({
                    message: null
                })
                let paso = res.data

                console.log(documento)

                if (documento.length > 0) {
                    this.crearDocumento(paso.id, documento).then((data) => {
                        paso.documento = [ data ]
                        this.props.handleSubmit(paso)
                    })
                }
                else {
                    this.props.handleSubmit(paso)
                }
            })
            .catch((err) => {
                console.log(err)
                this.setState({
                    message: err.response.data.data
                })
            })
    }


    handleSubmit = (e) => {

        e.preventDefault()

        const documentacion = this.documentacion.current.files

        // Request for documento

        this.crearPaso(documentacion)

    }

    render() {

        return (
            <div>
                {this.state.message ? (
                    <Alert variant="danger">{this.state.message}</Alert>
                ) : null}
                <Form encType="multipart/form-data" className='my-2'
                    onSubmit={this.handleSubmit}>

                    <Form.Group controlId="formOrdenNuevoPaso">
                        <Form.Control
                            required
                            type="number"
                            name="orden"
                            placeholder="Orden nuevo paso"
                            ref={orden => this.orden = orden}
                        />
                    </Form.Group>
                    <Form.Group controlId="formNombreNuevoPaso">
                        <Form.Control
                            required
                            type="text"
                            name="nombre"
                            placeholder="Nombre del nuevo Paso"
                            ref={nombre => this.nombre = nombre}
                        />
                    </Form.Group>
                    <Form.Group controlId="formDescripcionNuevoPaso">
                        <Form.Control
                            as="textarea"
                            name="descripcion"
                            placeholder="Descripción"
                            ref={descripcion => this.descripcion = descripcion}
                        />
                    </Form.Group>
                    <Form.Group controlId="formDocumentoNuevoPaso">
                        <Form.Control
                            type="file"
                            name="documentacion"
                            ref={this.documentacion}
                        />
                    </Form.Group>
                    <Button type="submit" variant="success">Crear</Button>
                </Form>
            </div>
        );
    }
}

export default PasoForm;
