import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal'

import TramiteForm from 'components/TramiteForm/TramiteForm'

class TramiteEditModal extends Component {
    render() {
        return (
            <div>
                <Modal onHide={this.props.onHide} show={this.props.show}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.tramite.titulo}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <TramiteForm tramite={this.props.tramite} handleSubmit={this.props.handleSubmit}></TramiteForm>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

export default TramiteEditModal;
