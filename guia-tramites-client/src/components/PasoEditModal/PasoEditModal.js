import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal'

import PasoForm from 'components/PasoForm/PasoForm'

class PasoEditModal extends Component {
    render() {
        return (
            <div>
                <Modal {...this.props}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.paso.nombre}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <PasoForm paso={this.props.paso} handleSubmit={this.props.handleSubmit}/>
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

export default PasoEditModal;
