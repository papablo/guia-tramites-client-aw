import React, { Component } from 'react';

import ListGroup from 'react-bootstrap/ListGroup'

class PasoList extends Component {

    state = {
        open:false
    }


    render() {
        const PasoItem = this.props.pasoComponent

        console.log(this.props.pasos)
        return (
            <div>
                <ListGroup>
                    {this.props.pasos.map((paso)=>{
                        return <PasoItem key={paso.orden} paso={paso} handleDelete={this.props.handleDelete}></PasoItem>
                    })}
                </ListGroup>

            </div>
        );
    }
}

export default PasoList;
