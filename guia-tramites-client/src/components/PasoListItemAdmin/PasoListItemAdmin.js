import React, { Component } from 'react';

import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'

import axios from 'axios'


import { FaTrash } from 'react-icons/fa'
import AuthHelper from 'components/AuthHelper/AuthHelper';

class PasoListItemAdmin extends Component {

    documento = () => {
        console.log(this.props)
        if (this.props.paso.documento) {
            if (this.props.paso.documento.length > 0) {
                return (
                    <div>
                        <hr />
                        Documentación
                    <ul>
                            {this.props.paso.documento.map((documento) => {
                                return (
                                    <li>
                                        <a target="_blank" href={process.env.REACT_APP_API_URL + '' + documento.link}>{documento.nombre}</a>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>)
            }
        }
    }



    handleDelete = () => {

        const authHelper = new AuthHelper()
        axios.delete(process.env.REACT_APP_API_URL + '/api/paso/' + this.props.paso.id, authHelper.get_auth_headers())
            .then((res) => {
                this.props.handleDelete(this.props.paso.id)
            })
            .catch((err) => {
                console.log(err.response)
            })

    }

    render() {

        return (
            <div>
                <ListGroup.Item >
                    <div>
                        {this.props.paso.id} - {this.props.paso.orden} - {this.props.paso.nombre}
                    </div>
                    <hr />
                    {this.props.paso.descripcion}

                    <hr />

                    {this.documento()}

                    <hr />

                    <Button onClick={this.handleDelete} variant="danger">
                        <span className="mr-2">
                            Eliminar paso
                    </span>
                        <FaTrash></FaTrash>
                    </Button>

                </ListGroup.Item>

            </div>
        );
    }
}

export default PasoListItemAdmin;
