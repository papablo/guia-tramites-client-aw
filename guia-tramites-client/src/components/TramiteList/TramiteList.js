import React, { Component } from 'react';

import ListGroup from 'react-bootstrap/ListGroup'
import FormControl from 'react-bootstrap/FormControl'

class TramiteList extends Component {

    state = {
        busqueda:''
    }

    render() {
        const inputProc = (e) =>{
            const inputValue = e.target.value

            this.setState({busqueda:inputValue})

        }

        const ListItem = this.props.itemComponent

        const getTramites = () => {
            const filtered = this.props.tramites.filter((tramite)=>{
                return tramite.titulo.toLowerCase().includes(this.state.busqueda.toLowerCase())
            })

            return filtered

        }
        return (
            <ListGroup>
                <FormControl className="my-3" onChange={inputProc} placeholder={"Buscar trámite"}></FormControl>
                {
                    getTramites().map(( tramite ) => {
                        return <ListItem key={tramite.id} tramite={tramite} handleDelete={this.props.handleDelete}></ListItem>
                    })
                }
            </ListGroup>
        )
    }
}

export default TramiteList;
